# Implementando [Jogo da Velha](https://pt-br.reactjs.org/tutorial/tutorial.html#inspecting-the-starter-code) em [React](https://pt-br.reactjs.org/)!

[![node](https://img.shields.io/badge/node-8.10.0-green.svg)](<[https://nodejs.org/en/download/](https://nodejs.org/en/download/)>)
[![yarn](https://img.shields.io/badge/yarn-1.16.0-blue.svg)](<[https://yarnpkg.com/pt-BR/docs/install#debian-stable](https://yarnpkg.com/pt-BR/docs/install#debian-stable)>)
[![create-react-app](https://img.shields.io/badge/create--react--app-3.0.0-orange.svg)](<[https://tableless.com.br/criando-sua-aplicacao-react-em-2-minutos/](https://tableless.com.br/criando-sua-aplicacao-react-em-2-minutos/)>)

O código foi documentado, explicando o que cada parte faz.

### Para baixar dependências:

    yarn install

### Para executar:

    yarn start

URL: [http://localhost:3000/](http://localhost:3000/)

## Para gerar build:

    yarn build

### Aluno: Jônathas Assunção Alves - 201613279
