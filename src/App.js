import React from 'react';
import "./App.css";
import Game from "./components/Game";

// Import o componente principal do Jogo
function App() {
  return (
    <Game/>
  );
}

export default App;
