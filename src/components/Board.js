import React from "react";
import Square from "./Square";

// Tabuleiro do jogo
class Board extends React.Component {
  
  // Rendezira um 'X' ou 'O' no quadrado do tabuleiro, quando clica em cima de algum quadrado.
  renderSquare(i) {
    return (
      <Square
        value={this.props.squares[i]}
        onClick={() => this.props.onClick(i)}
      />
    );
  }

  render() {
    // Monta o tabuleiro e define a posição de cada quadrado no tabuleiro
    return (
      <div>
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
}

export default Board;
