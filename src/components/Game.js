import React from "react";
import Board from "./Board";
import calculateWinner from "../utils/util";


// Componente principal
class Game extends React.Component {

  // Define contrutor do game
  constructor(props) {
    super(props);
    this.state = {

      // Armazena no status do Game o histórico de cada rodada
      history: [
        {
          // Limpa o array antes de armazenar cada histórico das rodadas
          squares: Array(9).fill(null)
        }
      ],

      // Define qual é a rodada
      stepNumber: 0,

      // Define se começa com 'X' ou 'O'
      xIsNext: true
    };
  }

  handleClick(i) {

    // Armazena a rodada atual ao histórico
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();

    // Verifica se houve ganhador na rodada
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = this.state.xIsNext ? "X" : "O";
    this.setState({

      // Atualiza o histórico com a rodada atual
      history: history.concat([
        {
          squares: squares
        }
      ]),

      // Define qual será o próximo jogador
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext
    });
  }

  jumpTo(step) {
    // Recupera uma rodada no histórico e exibe no tabuleiro
    this.setState({
      stepNumber: step,
      xIsNext: step % 2 === 0
    });
  }

  render() {

    // Gera o histórico da rodada atual
    const history = this.state.history;
    const current = history[this.state.stepNumber];

    //Verifica se houve vencedor nessa rodada
    const winner = calculateWinner(current.squares);

    // Exibe as opções de recuperar um histórico
    const moves = history.map((step, move) => {
      const desc = move ? "Movimento #" + move : "Começar Jogo";
      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      );
    });

    // Se houver vencedor, exibe o vencedor... Senão exibe qual será o próximo jogador!
    let status;
    if (winner) {
      status = "Vencedor: " + winner;
    } else {
      status = "Proximo Jogador: " + (this.state.xIsNext ? "X" : "O");
    }

    return (
      <div className="game">
        <div className="game-board">
          <Board squares={current.squares} onClick={i => this.handleClick(i)} />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
}

export default Game;
