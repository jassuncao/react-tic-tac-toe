import React from "react";

// Função que define 'X' ou 'O' em cada quadrado do tabuleiro
function Square(props) {
  return (
    <button className="square" onClick={props.onClick}>
      {props.value}
    </button>
  );
}

export default Square;
